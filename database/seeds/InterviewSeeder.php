<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'InterviewDate' => Carbon::now(),
                'Summary' => 'the candidate did really good',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'InterviewDate' => Carbon::now(),
                'Summary' => 'the candidate was not bad',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                      
            ]);   
    }
}

#test if pushes

