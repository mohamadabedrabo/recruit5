@extends('layouts.app')

@section('title', 'Interviews')

@section('content')
<div><a href =  "{{url('/candidates/createinterview')}}"> Add new Interview</a></div>
<h1>List of Interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Date</th><th>Summary</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->InterviewDate}}</td>
            <td>{{$interview->Summary}}</td>                                                            
        </tr>
    @endforeach
</table>
@endsection
